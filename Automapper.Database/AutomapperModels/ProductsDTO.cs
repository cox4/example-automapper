﻿namespace Automapper.Database
{
    public class ProductsDTO
    {
        public int ProductId { get; set; }
        public string Category { get; set; }
        public string Supplier { get; set; }
        public string Product { get; set; }
        public string Price { get; set; }
        public int InStock { get; set; }
        public int OnOrder { get; set; }
        public string Qunantity { get; set; }
        public bool Discontinued { get; set; }
    }
}
