﻿namespace Automapper.Database
{
    public class Products
    {
        public int ProductId { get; set; }
        public int SupplierId { get; set; }
        public int CategoryId { get; set; }
        public string ProductName { get; set; }
        public string UnitPrice { get; set; }
        public int UnitsInStock { get; set; }
        public int UnitsOnOrder { get; set; }
        public string QuantityPerUnit { get; set; }
        public int ReorderLevel { get; set; }
        public bool Discontinued { get; set; }
        public Suppliers Supplier { get; set; }
        public Categories Category { get; set; }
    }
}
