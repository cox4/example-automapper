﻿using Automapper.Database;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automapper.MVC.Controllers
{
    public class OrdersController : Controller
    {
        /// <summary>
        /// Number of records to show inside grid
        /// </summary>
        private readonly int _limit = 10;

        // GET: Orders
        public ActionResult Index()
        {
            var orders = Services.GetOrders().OrderByDescending(colum => colum.OrderDate).Take(_limit).ToList();
            var ordersDto = Mapper.Map<IEnumerable<OrdersDTO>>(orders);

            return View(ordersDto);
        }
    }
}