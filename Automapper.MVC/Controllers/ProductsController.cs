﻿using Automapper.Database;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automapper.MVC.Controllers
{
    public class ProductsController : Controller
    {
        /// <summary>
        /// Number of records to show inside grid
        /// </summary>
        private readonly int _limit = 10;

        // GET: Products
        public ActionResult Index()
        {
            var products = Services.GetProducts().OrderBy(colum => colum.ProductName).Take(_limit).ToList();
            var productsDto = Mapper.Map<IEnumerable<ProductsDTO>>(products);

            return View(productsDto);
        }
    }
}