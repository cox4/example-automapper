﻿namespace Automapper.WindowsForms
{
    using Automapper.Database;
    using AutoMapper;

    public static class AutomapperConfig
    {
        public static void Register()
        {
            // Initialization is the preferred mode of configuring AutoMapper, 
            // and should be done once per AppDomain:
            Mapper.Initialize(configuration =>
            {
                // Profiles folder
                configuration.AddProfile<ProductsProfile>();
                configuration.AddProfile<OrdersProfile>();
            });
        }
    }
}
