﻿namespace Automapper.WindowsForms
{
    using Automapper.Database;
    using AutoMapper;
    using System;

    class OrdersProfile : Profile
    {
        protected override void Configure()
        {
            // This will map Products entity to ProductsDTO class
            Mapper.CreateMap<Orders, OrdersDTO>()
                // Employee.FirstName + Employee.LastName => Employee
                .ForMember(orderDto => orderDto.Employee, option => option
                    .MapFrom(order => string.Format("{0} {1}", order.Employee.FirstName, order.Employee.LastName)))
                // Customer.ContactName + Customer.CompanyName => Customer
                .ForMember(orderDto => orderDto.Customer, map => map
                    .MapFrom(order => string.Format("{0} ({1})", order.Customer.ContactName, order.Customer.CompanyName)));
        }

        public override string ProfileName
        {
            get { return this.GetType().Name; }
        }
    }
}
