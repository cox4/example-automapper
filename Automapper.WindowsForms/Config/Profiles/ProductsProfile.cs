﻿namespace Automapper.WindowsForms
{
    using Automapper.Database;
    using AutoMapper;
    using System;

    class ProductsProfile : Profile
    {
        protected override void Configure()
        {
            // This will map Products entity to ProductsDTO class
            Mapper.CreateMap<Products, ProductsDTO>()
                // Category.CategoryName => Category
                .ForMember(productDto => productDto.Category, option => option
                    .MapFrom(product => product.Category.CategoryName))
                // Supplier.CompanyName + Supplier.ContactName => Supplier
                .ForMember(productDto => productDto.Supplier, map => map
                    .MapFrom(product => string.Format("{0} ({1})", product.Supplier.CompanyName, product.Supplier.ContactName)))
                // ProductName => Product
                .ForMember(productDto => productDto.Product, map => map
                    .MapFrom(product => product.ProductName))
                // UnitsInStock => InStock
                .ForMember(productDto => productDto.InStock, option => option
                    .MapFrom(product => product.UnitsInStock))
                // UnitsOnOrder => OnOrder
                .ForMember(productDto => productDto.OnOrder, option => option
                    .MapFrom(product => product.UnitsOnOrder))
                // UnitPrice => Price
                .ForMember(productDto => productDto.Price, option => option
                    .MapFrom(product => product.UnitPrice))
                // QunantityPerUnit => Quantity
                .ForMember(productDto => productDto.Qunantity, option => option
                    .MapFrom(product => product.QuantityPerUnit));
        }

        public override string ProfileName
        {
            get { return this.GetType().Name; }
        }
    }
}
