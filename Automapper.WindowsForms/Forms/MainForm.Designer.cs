﻿namespace Automapper.WindowsForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuContainer = new System.Windows.Forms.MenuStrip();
            this.menuProducts = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOrders = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEmployees = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCustomers = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSuppliers = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusReport = new System.Windows.Forms.ToolStripStatusLabel();
            this.gridContainer = new System.Windows.Forms.Panel();
            this.dataGridDatabase = new System.Windows.Forms.DataGridView();
            this.dataGridAutomapper = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuContainer.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.gridContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDatabase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAutomapper)).BeginInit();
            this.SuspendLayout();
            // 
            // menuContainer
            // 
            this.menuContainer.BackColor = System.Drawing.SystemColors.HighlightText;
            this.menuContainer.GripMargin = new System.Windows.Forms.Padding(0);
            this.menuContainer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuProducts,
            this.menuOrders,
            this.menuEmployees,
            this.menuCustomers,
            this.menuSuppliers});
            this.menuContainer.Location = new System.Drawing.Point(0, 0);
            this.menuContainer.Name = "menuContainer";
            this.menuContainer.Padding = new System.Windows.Forms.Padding(0);
            this.menuContainer.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuContainer.Size = new System.Drawing.Size(884, 33);
            this.menuContainer.TabIndex = 0;
            this.menuContainer.Text = "menuStrip1";
            // 
            // menuProducts
            // 
            this.menuProducts.Margin = new System.Windows.Forms.Padding(2);
            this.menuProducts.Name = "menuProducts";
            this.menuProducts.Padding = new System.Windows.Forms.Padding(5);
            this.menuProducts.Size = new System.Drawing.Size(68, 29);
            this.menuProducts.Text = "Products";
            this.menuProducts.Click += new System.EventHandler(this.menuProducts_Click);
            // 
            // menuOrders
            // 
            this.menuOrders.Margin = new System.Windows.Forms.Padding(2);
            this.menuOrders.Name = "menuOrders";
            this.menuOrders.Padding = new System.Windows.Forms.Padding(5);
            this.menuOrders.Size = new System.Drawing.Size(56, 29);
            this.menuOrders.Text = "Orders";
            this.menuOrders.Click += new System.EventHandler(this.menuOrders_Click);
            // 
            // menuEmployees
            // 
            this.menuEmployees.Margin = new System.Windows.Forms.Padding(2);
            this.menuEmployees.Name = "menuEmployees";
            this.menuEmployees.Padding = new System.Windows.Forms.Padding(5);
            this.menuEmployees.Size = new System.Drawing.Size(78, 29);
            this.menuEmployees.Text = "Employees";
            this.menuEmployees.Click += new System.EventHandler(this.menuEmployees_Click);
            // 
            // menuCustomers
            // 
            this.menuCustomers.Margin = new System.Windows.Forms.Padding(2);
            this.menuCustomers.Name = "menuCustomers";
            this.menuCustomers.Padding = new System.Windows.Forms.Padding(5);
            this.menuCustomers.Size = new System.Drawing.Size(78, 29);
            this.menuCustomers.Text = "Customers";
            this.menuCustomers.Click += new System.EventHandler(this.menuCustomers_Click);
            // 
            // menuSuppliers
            // 
            this.menuSuppliers.Margin = new System.Windows.Forms.Padding(2);
            this.menuSuppliers.Name = "menuSuppliers";
            this.menuSuppliers.Padding = new System.Windows.Forms.Padding(5);
            this.menuSuppliers.Size = new System.Drawing.Size(69, 29);
            this.menuSuppliers.Text = "Suppliers";
            this.menuSuppliers.Click += new System.EventHandler(this.menuSuppliers_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusReport});
            this.statusStrip1.Location = new System.Drawing.Point(2, 504);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(880, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusReport
            // 
            this.statusReport.Name = "statusReport";
            this.statusReport.Size = new System.Drawing.Size(95, 17);
            this.statusReport.Text = "Choose action ...";
            // 
            // gridContainer
            // 
            this.gridContainer.Controls.Add(this.label2);
            this.gridContainer.Controls.Add(this.label1);
            this.gridContainer.Controls.Add(this.dataGridAutomapper);
            this.gridContainer.Controls.Add(this.statusStrip1);
            this.gridContainer.Controls.Add(this.dataGridDatabase);
            this.gridContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridContainer.Location = new System.Drawing.Point(0, 33);
            this.gridContainer.Margin = new System.Windows.Forms.Padding(0);
            this.gridContainer.Name = "gridContainer";
            this.gridContainer.Padding = new System.Windows.Forms.Padding(2);
            this.gridContainer.Size = new System.Drawing.Size(884, 528);
            this.gridContainer.TabIndex = 1;
            // 
            // dataGridDatabase
            // 
            this.dataGridDatabase.AllowUserToAddRows = false;
            this.dataGridDatabase.AllowUserToDeleteRows = false;
            this.dataGridDatabase.AllowUserToOrderColumns = true;
            this.dataGridDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridDatabase.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridDatabase.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridDatabase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridDatabase.Location = new System.Drawing.Point(2, 25);
            this.dataGridDatabase.Margin = new System.Windows.Forms.Padding(0);
            this.dataGridDatabase.MultiSelect = false;
            this.dataGridDatabase.Name = "dataGridDatabase";
            this.dataGridDatabase.ReadOnly = true;
            this.dataGridDatabase.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridDatabase.Size = new System.Drawing.Size(880, 200);
            this.dataGridDatabase.TabIndex = 0;
            // 
            // dataGridAutomapper
            // 
            this.dataGridAutomapper.AllowUserToAddRows = false;
            this.dataGridAutomapper.AllowUserToDeleteRows = false;
            this.dataGridAutomapper.AllowUserToOrderColumns = true;
            this.dataGridAutomapper.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridAutomapper.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridAutomapper.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridAutomapper.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAutomapper.Location = new System.Drawing.Point(2, 246);
            this.dataGridAutomapper.Margin = new System.Windows.Forms.Padding(0);
            this.dataGridAutomapper.MultiSelect = false;
            this.dataGridAutomapper.Name = "dataGridAutomapper";
            this.dataGridAutomapper.ReadOnly = true;
            this.dataGridAutomapper.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridAutomapper.Size = new System.Drawing.Size(880, 258);
            this.dataGridAutomapper.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.label1.Size = new System.Drawing.Size(183, 21);
            this.label1.TabIndex = 3;
            this.label1.Text = "Database entities are show bellow:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 225);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.label2.Size = new System.Drawing.Size(334, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "Using automapper we\'ve converted data to our custom DTO class:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.gridContainer);
            this.Controls.Add(this.menuContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuContainer;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Automapper - WindowsApplication";
            this.menuContainer.ResumeLayout(false);
            this.menuContainer.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.gridContainer.ResumeLayout(false);
            this.gridContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDatabase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAutomapper)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuContainer;
        private System.Windows.Forms.ToolStripMenuItem menuProducts;
        private System.Windows.Forms.ToolStripMenuItem menuEmployees;
        private System.Windows.Forms.ToolStripMenuItem menuCustomers;
        private System.Windows.Forms.ToolStripMenuItem menuSuppliers;
        private System.Windows.Forms.ToolStripMenuItem menuOrders;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusReport;
        private System.Windows.Forms.Panel gridContainer;
        private System.Windows.Forms.DataGridView dataGridAutomapper;
        private System.Windows.Forms.DataGridView dataGridDatabase;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;

    }
}

