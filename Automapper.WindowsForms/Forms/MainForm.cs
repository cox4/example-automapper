﻿namespace Automapper.WindowsForms
{
    using Automapper.Database;
    using AutoMapper;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using System.Linq;

    public partial class MainForm : Form
    {
        /// <summary>
        /// Number of records to show inside grid
        /// </summary>
        private readonly int _limit = 10;

        public MainForm()
        {
            InitializeComponent();
        }

        private void menuProducts_Click(object sender, System.EventArgs e)
        {
            if (dataGridDatabase.DataSource != null || dataGridAutomapper.DataSource != null)
            {
                dataGridDatabase.DataSource = null;
                dataGridAutomapper.DataSource = null;
            }

            var products = Services.GetProducts().OrderBy(colum => colum.ProductName).Take(_limit).ToList();
            var productsDto = Mapper.Map<IEnumerable<ProductsDTO>>(products);

            dataGridDatabase.DataSource = products;
            dataGridAutomapper.DataSource = productsDto;

            statusReport.Text = "Loaded PRODUCTS from in-memory database...";
        }

        private void menuOrders_Click(object sender, System.EventArgs e)
        {
            if (dataGridDatabase.DataSource != null || dataGridAutomapper.DataSource != null)
            {
                dataGridDatabase.DataSource = null;
                dataGridAutomapper.DataSource = null;
            }

            var orders = Services.GetOrders().OrderByDescending(colum => colum.OrderDate).Take(_limit).ToList();
            var ordersDto = Mapper.Map<IEnumerable<OrdersDTO>>(orders);

            dataGridDatabase.DataSource = orders;
            dataGridAutomapper.DataSource = ordersDto;

            statusReport.Text = "Loaded ORDERS from in-memory database...";
        }

        private void menuEmployees_Click(object sender, System.EventArgs e)
        {
            if (dataGridDatabase.DataSource != null || dataGridAutomapper.DataSource != null)
            {
                dataGridDatabase.DataSource = null;
                dataGridAutomapper.DataSource = null;
            }

            var employees = Services.GetEmployees().OrderBy(colum => colum.LastName).Take(_limit).ToList();
            //var employeesDto = Mapper.Map<IEnumerable<ProductsDTO>>(employees);

            dataGridDatabase.DataSource = employees;
            //dataGridAutomapper.DataSource = employeesDto;
            statusReport.Text = "Loaded EMPLOYEES from in-memory database...";
        }

        private void menuCustomers_Click(object sender, System.EventArgs e)
        {
            if (dataGridDatabase.DataSource != null || dataGridAutomapper.DataSource != null)
            {
                dataGridDatabase.DataSource = null;
                dataGridAutomapper.DataSource = null;
            }

            var customers = Services.GetCustomers().OrderBy(colum => colum.CompanyName).Take(_limit).ToList();
            //var employeesDto = Mapper.Map<IEnumerable<ProductsDTO>>(employees);

            dataGridDatabase.DataSource = customers;
            //dataGridAutomapper.DataSource = employeesDto;
            statusReport.Text = "Loaded CUSTOMERS from in-memory database...";
        }

        private void menuSuppliers_Click(object sender, System.EventArgs e)
        {
            if (dataGridDatabase.DataSource != null || dataGridAutomapper.DataSource != null)
            {
                dataGridDatabase.DataSource = null;
                dataGridAutomapper.DataSource = null;
            }

            var suppliers = Services.GetSuppliers().OrderBy(colum => colum.Region).Take(_limit).ToList();
            //var ordersDto = Mapper.Map<IEnumerable<ProductsDTO>>(orders);

            dataGridDatabase.DataSource = suppliers;
            //dataGridAutomapper.DataSource = ordersDto;

            statusReport.Text = "Loaded SUPPLIERS from in-memory database...";
        }
    }
}
